# Artivatic

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
##Node Module Installation
Here I am not giving node_modules folder so please run npm install for installing all dependencies.

##Data Handle
1-Taken data ,whose lpg_primary is not undefined
2-Green means true lpg_primary
3-Red means false lpg_primary
4-Using behaviuor subject for sharing API data across app,using service
5-Interface created for contratc person with API'response
6-Bootstrap-4.3.1 Version Used
7-Angular-7.2.15 Verion Used

##GeoChart+Drill with Custom Pipe Component
When App will open default page will come -this page contain following functionality
1-Point Lat-Long on Google Maps with a custom marker, the color of the marker based on the lpg_primary
2-On hover over a marker, standard pop-up on the map which shows owner_type, geocode_status, cards_accepted, fuel_type_code
3-The map followed by a table which lists out all the important entries 
4-Filter pipe for the table based on fule_type_code, cards_accepted, station_name, lpg_primary
5-put value in search box for checking functionality of custom pipe
6-onhover on marks for pop-up fucntionality

##Chart with Coupled Drill Component
1-The map followed by a table which lists out all the important entries and the sub-standard entries are available only if the user wants to see (onclick of href link-you will be able to see subentries).
2-A chart (of your choice and which relates close to the representative value) which outlines the number of times a city was mentioned.
3-Use of grid system .
4-When user will serach the city map will be change and if user will click on blue link the one more table will be open with sub entries


##Note
I was not able to do the task in ofc that's why it taken little bit time,if any thing pending please let me know
Thank you for giving me opportunity. 

## Further help



To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
