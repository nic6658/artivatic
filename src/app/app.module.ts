import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { HttpClientModule } from '@angular/common/http';
import { TableModule } from 'primeng/table';
import { GridChartComponent } from './grid-chart/grid-chart.component';
import { ChartsModule } from 'ng2-charts';
import { GeoChartComponent } from './geo-chart/geo-chart.component';
import { DrillWithPipeComponent } from './drill-with-pipe/drill-with-pipe.component';
import { CustomFilter } from './custom-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    CustomFilter,
    GridChartComponent,
    GeoChartComponent,
    DrillWithPipeComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    TableModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAH_1a6b9tu1BpN01tg6DRQaOKjCA5yIDE'
    })
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
]
})
export class AppModule { }
