
/*
Developer-Akash
Verion-1.0
Functionality-This is GeoChart Component,
Point Lat-Long on Google Maps with a custom marker, the color of the marker based on the lpg_primary.
On hover over a marker, standard pop-up on the map which shows owner_type, geocode_status, cards_accepted, fuel_type_code
In it ,we are calling demo Api for chart prepartion,in result we are making one check that,
 if lpg_primary is defined then we will make an array of chart data.
we are also implementing an interface for contract with data
name of interface is Maker,exist in share folder

*/
import { Component, ViewChild,Input } from "@angular/core";
import { MouseEvent } from "@agm/core";
import { Router } from "@angular/router";
import { ViewService } from "../../shared/view.service";
import { Marker } from '../../shared/Marker';
@Component({
  selector: "app-geo-chart",
  templateUrl: "./geo-chart.component.html",
  styleUrls: ["./geo-chart.component.css"]
})


export class GeoChartComponent  {
  // google maps zoom level
  zoom: number = 10;//we can set Zoom level of chart
  markers: Marker[] = [];
  constructor(private viewService: ViewService, private router: Router) {
    this.viewService.routerVAl = "/";
    this.viewService.get_products().subscribe((res: any[]) => {
      res.forEach(element => {
        if (element.hasOwnProperty("lpg_primary")) {
          this.markers.push({
            lat: element.location.latitude,
            lng: element.location.longitude,
            label: element.city,
            iconColor: element.lpg_primary,
            ownerType: element.owner_type_code,
            geocodeStatus: element.geocode_status,
            cardsAccepted: element.cards_accepted,
            fuelTypeCode: element.fuel_type_code,
            stationName: element.station_name
            // draggable: true
          });
        }
      });
      this.viewService.setData(this.markers)
    });
 }

  // initial center position for the map
  lat: number = 41.850832;
  lng: number = -87.750927;


  //this code is for open pop-up for,when user will onhover ,it will open
  mouseOver(infoWindow, gm) {
    if (gm.lastOpen && gm.lastOpen.isOpen) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;
   infoWindow.open();
  }

  //this code is for,when user remove mouseover ,pop-up window will close
  onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
  }

}




