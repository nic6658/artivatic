
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'customFilter'})

export class CustomFilter implements PipeTransform {
  transform(items:  Array<any>, fuelSearch: string, cardsSearch: string, stationSearch: string , lpgSearch: string){
      if (items && items.length){
          var mko= items.filter(item =>{
              if (fuelSearch && item.fuelTypeCode.toLowerCase().indexOf(fuelSearch.toLowerCase()) === -1){
                  return false;
              }
              if (cardsSearch && item.cardsAccepted.toLowerCase().indexOf(cardsSearch.toLowerCase()) === -1){
                  return false;
              }
              if (stationSearch && item.stationName.toLowerCase().indexOf(stationSearch.toLowerCase()) === -1){
                  return false;
              }
              if (lpgSearch && item.iconColor.toLowerCase().indexOf(lpgSearch.toLowerCase()) === -1){
                return false;
            }
              return true;
         })
return mko;

      }
      else{
          return items;
      }
  }
}
