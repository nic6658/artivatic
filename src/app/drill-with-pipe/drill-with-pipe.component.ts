/*
Developer-Akash
Verion-1.0
Functionality-This is Drill Component with custom pipe filter functionality
When user will give value in input box,then drill data will filter
Filter pipe for the table based on fule_type_code, cards_accepted, station_name, lpg_primary
In it ,we are calling demo Api for chart prepartion,in result we are making one check that,
 if lpg_primary is defined then we will make an array of chart data.
we are also implementing an interface for contract with data
name of interface is Maker,exist in share folder

*/

import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ViewService } from '../../shared/view.service';
import { Marker } from '../../shared/Marker';
@Component({
  selector: 'app-drill-with-pipe',
  templateUrl: './drill-with-pipe.component.html',
  styleUrls: ['./drill-with-pipe.component.css']
})
export class DrillWithPipeComponent  {

   markers: Marker[] = [];
  fueltypecode;
  cardsaccepted;
  stationname;
  lpgprimary;
  showMainComponent: boolean = false;
  data;
  loading = true;
  constructor(private viewService: ViewService, private router: Router) {
    this.viewService.routerVAl = "/";

    viewService.apiData$.subscribe((data)=>{
     this.markers=data;
     this.loading = false;
    })


  }


}



