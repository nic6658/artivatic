import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrillWithPipeComponent } from './drill-with-pipe.component';

describe('DrillWithPipeComponent', () => {
  let component: DrillWithPipeComponent;
  let fixture: ComponentFixture<DrillWithPipeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrillWithPipeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrillWithPipeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
