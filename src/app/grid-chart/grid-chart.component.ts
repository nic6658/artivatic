/*
Developer-Akash
Verion-1.0
Functionality-This is Grid with Chart Component,
The map followed by a table which lists out all the important entries and the sub-standard entries are available only if the user wants to see (Hint: On click)
A chart (of your choice and which relates close to the representative value) which outlines the number of times a city was mentioned.
Use of grid system mandatory.
In it we are making funtionality--
chart and table data coupled ,when filter data will change then chart data will also change


Note--click on city Link for nested data
*/
import { Component, OnInit, ViewChild, Input } from "@angular/core";
import { Table, ScrollableView } from "primeng/components/table/table";
import { Router } from "@angular/router";
import { ViewService } from "../../shared/view.service";
import { ChartOptions, ChartType, ChartDataSets } from "chart.js";
import { Label } from "ng2-charts";
@Component({
  selector: "app-grid-chart",
  templateUrl: "./grid-chart.component.html",
  styleUrls: ["./grid-chart.component.css"]
})
export class GridChartComponent implements OnInit {
  cols: any[];
  drillData: any[];

  @ViewChild("dt") dt: Table;
  rowGroupMetadata: any;
  public barChartOptions: ChartOptions = {
    responsive: true
  };
  public barChartLabels: Label[] = [];
  public barChartType: ChartType = "bar";
  public barChartLegend = true;
  public barChartPlugins = [];
  loading = true;
  public barChartData: ChartDataSets[] = [];

  constructor(private router: Router, private viewService: ViewService) {
    this.viewService.routerVAl = this.router.url;
    this.tablePrparation();
  }

  ngOnInit() {}

  tablePrparation() {
    this.cols = [
      { field: "city", header: "City", width: 200 },
      { field: "cards_accepted", header: "Cards Accepted", width: 200 },
      {
        field: "intersection_directions",
        header: "Intersection Directions",
        width: 200
      },
      { field: "state", header: "State", width: 200 },
      { field: "station_name", header: "Station Name", width: 200 }
    ];

    this.viewService.get_products().subscribe((res: any[]) => {
      let drillArray = [];
      res.forEach((element, index) => {
        drillArray.push({
          rowId: index,
          city: element.city,
          cards_accepted: element.cards_accepted,
          intersection_directions: element.intersection_directions,
          state: element.state,
          station_name: element.station_name
        });
      });
      this.drillData = drillArray;

      this.chartPreparation(this.drillData);
      this.loading = false;
    });

    this.updateRowGroupMetaData();
  }

  chartPreparation(drillVar) {

    let chartJsonObject = this.mapToProp(drillVar, "city");
    let chartKeys = Object.keys(chartJsonObject);
    let labelArray = [];
    let chartPointArray = [];
    for (let label of chartKeys) {
      labelArray.push(label);
      chartPointArray.push(chartJsonObject[label]);
    }
    this.barChartLabels = labelArray;
    this.barChartData = [{ data: chartPointArray, label: "City Count" }];

   // console.log("mapAndCountByAge", this.mapToProp(this.drillData, "city"));

  }

  updateRowGroupMetaData() {
    this.rowGroupMetadata = {};
    if (this.drillData) {
      for (let i = 0; i < this.drillData.length; i++) {
        let rowData = this.drillData[i];
        let brand = rowData.brand;
        if (i == 0) {
          this.rowGroupMetadata[brand] = { index: 0, size: 1 };
        } else {
          let previousRowData = this.drillData[i - 1];
          let previousRowGroup = previousRowData.brand;
          if (brand === previousRowGroup) this.rowGroupMetadata[brand].size++;
          else this.rowGroupMetadata[brand] = { index: i, size: 1 };
        }
      }
    }
  }

  mapToProp(data, prop) {
    return data.reduce(
      (res, item) =>
        Object.assign(res, {
          [item[prop]]: 1 + (res[item[prop]] || 0)
        }),
      Object.create(null)
    );
  }

  filterEvent(dt) {
    console.log(dt.filteredValue);
    if(dt.filteredValue==null)
    {
      this.chartPreparation(this.drillData);
    }
    else
    {
      this.chartPreparation(dt.filteredValue);
      }

  }

  filterdata(val, fieldType, typeSearch) {
    this.dt.filter(val, fieldType, typeSearch);
    // console.log("asdsadsad"+this.dt.filteredValue);
  }
}
