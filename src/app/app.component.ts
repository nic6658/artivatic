import { Component, OnInit, ViewChild } from "@angular/core";
import { MouseEvent } from "@agm/core";
import { ViewService } from "../shared/view.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent implements OnInit {
  // google maps zoom level


  zoom: number = 10;
  markers: marker[] = [];
  showMainComponent: boolean = false;
  constructor(private viewService: ViewService, private router: Router) {
    this.viewService.routerVAl = "/";

  }

  // initial center position for the map
  lat: number = 41.850832;
  lng: number = -87.750927;


  clickNavigation(path) {
    if (path == "googlemap") {
      this.viewService.routerVAl = "/";
      this.router.navigate(["/"]);
    }

    if (path == "chartDrill") {
      this.viewService.routerVAl = "/gridChart";
      this.router.navigate(["/gridChart"]);
    }
  }

  ngOnInit() {

  }

  markerDragEnd(m: marker, $event: MouseEvent) {
    console.log("dragEnd", m, $event);
  }
  onMouseOut(infoWindow, $event: MouseEvent) {
    infoWindow.close();
  }

  mouseOver(infoWindow, gm) {
    if (gm.lastOpen && gm.lastOpen.isOpen) {
      gm.lastOpen.close();
    }

    gm.lastOpen = infoWindow;

    infoWindow.open();
  }
}

interface marker {
  lat: string;
  lng: string;
  label?: string;
  iconColor: string;
  ownerType: string;
  geocodeStatus: string;
  cardsAccepted: string;
  fuelTypeCode: string;
  stationName: string;
  //draggable: boolean;
}

export interface User {
  id;
  name;
  email;
}
