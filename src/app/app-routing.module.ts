import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridChartComponent } from './grid-chart/grid-chart.component';

const routes: Routes = [
  {
    path: 'gridChart',
    component: GridChartComponent
 }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
