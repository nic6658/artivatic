export interface Marker {
   lat: string;
  lng: string;
  label?: string;
  iconColor: string;
  ownerType: string;
  geocodeStatus: string;
  cardsAccepted: string;
  fuelTypeCode: string;
  stationName: string;
}
