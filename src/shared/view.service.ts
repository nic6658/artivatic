/*
Developer-Akash
Verion-1.0
Functionality-Service
Service will inject in to component using dependancy injection

*/

import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ViewService {
  baseUrl;
  routerVAl;
  markerArray;
  private apiData = new BehaviorSubject<any>(null);
  public apiData$ = this.apiData.asObservable();
  constructor(private httpClient: HttpClient) {}

  get_products() {
    this.baseUrl = "https://data.cityofchicago.org/resource/f7f2-ggz5.json?";
    return this.httpClient.get(this.baseUrl);
  }


  //code with error handling
  //get_products() {
  //  this.baseUrl = "https://data.cityofchicago.org/resource/f7f2-ggz5.json?";
    //return this.httpClient.get(this.baseUrl);

    //return this.httpClient.get<any>(this.baseUrl).pipe(map(response => {
    //  return true;
   // }), catchError((error: any) => {
   //   return observableThrowError(error);
   // }));



// here we set/change value of the observable
setData(data) {
  this.apiData.next(data);
}
}
